# Android Speedometer

This is a very simple and easy to use spedometer. Te speed is computed using only the acceleration with the following formula 'v = v0 + ΔT' and assuming that the initial velocity is 0.

![](https://media.giphy.com/media/ieeoXFufgW5vud03Ok/giphy.gif)