package com.sapikelio.android.speedometer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sapikelio.android.speedometer.model.VelocityStatus;

public class MainActivity extends AppCompatActivity {

    private TextView vel;
    private TextView status;

    Button exitButton;

    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            float velocity = intent.getFloatExtra(AccelerometerService.VELOCITY_TAG, 0);
            VelocityStatus status = (VelocityStatus) intent.getSerializableExtra(AccelerometerService.STATUS_TAG);
            updateText(velocity, status);
        }
    };

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        status = (TextView) findViewById(R.id.status);
        vel = (TextView) findViewById(R.id.vel);

        exitButton = (Button) findViewById(R.id.boton_salir);
        exitButton.setOnClickListener(myhandler1);

        Intent intent = new Intent(getBaseContext(), AccelerometerService.class);
        if (getIntent().getExtras() != null) {
            double accelerometerError = getIntent().getDoubleExtra(AccelerometerService.ACCELEROMETER_ERROR, 0);
            intent.putExtra(AccelerometerService.ACCELEROMETER_ERROR, accelerometerError);

            float initialVelocity = getIntent().getFloatExtra(AccelerometerService.INITIAL_VELOCITY_TAG, 0);
            intent.putExtra(AccelerometerService.INITIAL_VELOCITY_TAG, initialVelocity);
        }
        startService(intent);
    }


    View.OnClickListener myhandler1 = new View.OnClickListener() {
        public void onClick(View v) {
            stopService(new Intent(MainActivity.this, AccelerometerService.class));
            finish();
        }
    };


    public void updateText(double velocity, VelocityStatus statusValue) {
        status.setText(statusValue.getName());
        vel.setText(Double.toString(Math.round(velocity * 100) / 100.0) + " km/h");
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(AccelerometerService.BROADCAST_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }
}
