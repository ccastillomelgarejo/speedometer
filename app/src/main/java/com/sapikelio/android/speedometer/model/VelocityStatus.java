package com.sapikelio.android.speedometer.model;

/**
 * Created by Carlos on 03/02/2017.
 */

public enum VelocityStatus {
    STOP(0f, "Stopped"),
    WALK(1f, "Walking"),
    MARCH(3f, "Trotting"),
    RUN(6f, "Running"),
    SPRINT(12f, "Sprinting"),
    LAND_VEH(25f, "Terrestrial Veh."),
    AIR_VEH(170f, "Aerial Veh.");

    private Float defaultLowerBounds;
    private String name;

    VelocityStatus(Float defaultLowerBounds, String name) {
        this.defaultLowerBounds = defaultLowerBounds;
        this.name = name;
    }

    public Float getDefaultLowerBounds() {
        return defaultLowerBounds;
    }

    public void setDefaultLowerBounds(Float defaultLowerBounds) {
        this.defaultLowerBounds = defaultLowerBounds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
