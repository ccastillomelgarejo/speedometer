package com.sapikelio.android.speedometer.model;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * Created by ccastillo on 13/02/2017.
 */

public class Velocity {

    double accelerometerError;

    long timestamp;
    float velocityX;
    float velocityY;
    float velocityZ;

    float initialVelocity;

    public Velocity(float initialVelocity, double accelerometerError, long lastEvent,
                    float lastVelocityX, float lastVelocityY, float lastVelocityZ,
                    float lastAccerelationX, float lastAccerelationY, float lastAccerelationZ,
                    float accerelationX, float accerelationY, float accerelationZ) {
        this.accelerometerError = accelerometerError;
        this.timestamp = System.currentTimeMillis();

        this.initialVelocity = initialVelocity;

        this.velocityX = calculateVelocityFromAceleration(accerelationX, lastAccerelationX, lastVelocityX, lastEvent);
        this.velocityY = calculateVelocityFromAceleration(accerelationY, lastAccerelationY, lastVelocityY, lastEvent);
        this.velocityZ = calculateVelocityFromAceleration(accerelationZ, lastAccerelationZ, lastVelocityZ, lastEvent);
    }

    public double getAccelerometerError() {
        return accelerometerError;
    }

    public void setAccelerometerError(double accelerometerError) {
        this.accelerometerError = accelerometerError;
    }

    public float getVelocity() {
        return computeVelocityModule(velocityX, velocityY, velocityZ);
    }


    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public float getVelocityX() {
        return velocityX;
    }

    public void setVelocityX(float velocityX) {
        this.velocityX = velocityX;
    }

    public float getVelocityY() {
        return velocityY;
    }

    public void setVelocityY(float velocityY) {
        this.velocityY = velocityY;
    }

    public float getVelocityZ() {
        return velocityZ;
    }

    public void setVelocityZ(float velocityZ) {
        this.velocityZ = velocityZ;
    }

    public VelocityStatus getStatus() {
        VelocityStatus[] statuses = VelocityStatus.values();
        float velocityKMH = getVelocityKMH();
        for (int i = 0; i < statuses.length; i++) {
            float value = statuses[i].getDefaultLowerBounds();
            if ((i + 1) == statuses.length) {
                if (velocityKMH > value)
                    return statuses[i];
            } else {
                float nextValue = statuses[i + 1].getDefaultLowerBounds();
                if (velocityKMH > value && velocityKMH < nextValue)
                    return statuses[i];
            }
        }
        return statuses[0];
    }

    private float calculateVelocityFromAceleration(float acceleration, float lastAcceleration, float lastVelocity, long lastEvent) {
        long interval = (timestamp - lastEvent);
        return lastVelocity + ((float) ((acceleration-accelerometerError) - (lastAcceleration-accelerometerError)) * interval / 1000);
    }

    public float getVelocityKMH() {
        return (initialVelocity + getVelocity()) * 3.6f;
    }

    private float computeVelocityModule(float vX, float vY, float vZ) {
        return (float) sqrt(pow(vX, 2) + pow(vY, 2) + pow(vZ, 2));
    }


}
