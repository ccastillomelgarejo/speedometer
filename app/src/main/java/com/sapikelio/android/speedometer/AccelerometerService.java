package com.sapikelio.android.speedometer;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;

import com.sapikelio.android.speedometer.model.Velocity;

import static android.hardware.SensorManager.SENSOR_DELAY_NORMAL;

public class AccelerometerService extends Service implements SensorEventListener {

    public static final String ACCELEROMETER_ERROR = "ACCELEROMETER_ERROR";
    public static final String VELOCITY_TAG = "VELOCITY";
    public static final String INITIAL_VELOCITY_TAG = "INITIAL_VELOCITY";
    public static final String STATUS_TAG = "STATUS";
    public static final String BROADCAST_ACTION = "com.sapikelio.android.speedometer.ACCELEROMETER_CHANGED";

    private SensorManager sensorManager;
    private Sensor accelerometer;

    Long lastEvent;

    float lastVelocityX;
    float lastVelocityY;
    float lastVelocityZ;

    Float lastAccelerationX;
    Float lastAccelerationY;
    Float lastAccelerationZ;

    float initialVelocity;

    double accelerometerError;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            if (lastEvent != null) {
                Velocity velocity = new Velocity(initialVelocity,accelerometerError, lastEvent,
                        lastVelocityX, lastVelocityY, lastVelocityZ,
                        lastAccelerationX, lastAccelerationY, lastAccelerationZ,
                        sensorEvent.values[0],sensorEvent.values[1],sensorEvent.values[2] );

                lastEvent = velocity.getTimestamp();

                lastVelocityX = velocity.getVelocityX();
                lastVelocityY = velocity.getVelocityY();
                lastVelocityZ = velocity.getVelocityZ();

                Intent intent = new Intent(BROADCAST_ACTION);
                intent.putExtra(VELOCITY_TAG, velocity.getVelocityKMH());
                intent.putExtra(STATUS_TAG, velocity.getStatus());
                sendBroadcast(intent);
            }else{
                lastEvent=  System.currentTimeMillis();
            }

            lastAccelerationX = sensorEvent.values[0];
            lastAccelerationY = sensorEvent.values[1];
            lastAccelerationZ = sensorEvent.values[2];


        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getExtras() != null) {

            lastVelocityX = 0;
            lastVelocityY = 0;
            lastVelocityZ = 0;

            accelerometerError = intent.getDoubleExtra(ACCELEROMETER_ERROR, 0);
            initialVelocity = intent.getFloatExtra(INITIAL_VELOCITY_TAG, 0);

            lastEvent=null;

            sensorManager.registerListener(this, accelerometer, SENSOR_DELAY_NORMAL);

            Intent intentNotify = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intentNotify, 0);

            Notification noti = new Notification.Builder(getApplicationContext())
                    .setContentTitle("MIVELOCIDAD")
                    .setContentText("activo")
                    .setSmallIcon(R.drawable.ic_stat_name)
                    .setContentIntent(pendingIntent)
                    .build();

            startForeground(12345678, noti);
        }
        return Service.START_STICKY;
    }


    @Override
    public void onDestroy() {
        sensorManager.unregisterListener(this);
    }
}
