package com.sapikelio.android.speedometer;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import static android.hardware.SensorManager.SENSOR_DELAY_FASTEST;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class CalibrationActivity extends AppCompatActivity implements SensorEventListener {


    public static final String TAG = CalibrationActivity.class.getName();

    private SensorManager sensorManager;
    private Sensor accelerometer;

    long readingsNumber;
    double accelerationSum;

    Button initButton;
    Button continueButton;
    ProgressBar progressBar;
    TextView textExplanation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calibration);

        if (isServiceRunning(AccelerometerService.class)) {
            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            startActivity(intent);
        }

        readingsNumber = 0;
        accelerationSum = 0;

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

        textExplanation= (TextView) findViewById(R.id.text_explanation);

        initButton = (Button) findViewById(R.id.boton_calibrar);
        initButton.setOnClickListener(myhandler1);

        continueButton = (Button) findViewById(R.id.boton_continuar);
        continueButton.setOnClickListener(myhandler2);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        if (accelerometer == null) {
            Log.e(TAG, "This device hasn't got an accelerometer sensor");
            finish();
        }
    }


    View.OnClickListener myhandler1 = new View.OnClickListener() {
        public void onClick(View v) {
            initButton.setVisibility(GONE);
            progressBar.setVisibility(VISIBLE);
            sensorManager.registerListener(CalibrationActivity.this, accelerometer, SENSOR_DELAY_FASTEST);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    sensorManager.unregisterListener(CalibrationActivity.this);
                    progressBar.setVisibility(GONE);
                    continueButton.setVisibility(VISIBLE);
                    textExplanation.setText("The device has been calibrated succesfully. Press the button to start.");
                }
            }, 5000);
        }
    };

    View.OnClickListener myhandler2 = new View.OnClickListener() {
        public void onClick(View v) {
            double accelerometerError = accelerationSum / readingsNumber;
            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            intent.putExtra(AccelerometerService.ACCELEROMETER_ERROR, accelerometerError);

            float vIni = 0f;
            intent.putExtra(AccelerometerService.INITIAL_VELOCITY_TAG, vIni);
            startActivity(intent);
        }
    };

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {

            float accerelationX = sensorEvent.values[0];
            float accerelationY = sensorEvent.values[1];
            float accerelationZ = sensorEvent.values[2];

            accelerationSum += computeAccelerationModule(accerelationX, accerelationY, accerelationZ);
            readingsNumber++;
        }
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    private float computeAccelerationModule(float aX, float aY, float aZ) {
        return (float) sqrt(pow(aX, 2) + pow(aY, 2) + pow(aZ, 2));
    }

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

        // Loop through the running services
        for (ActivityManager.RunningServiceInfo service : activityManager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                // If the service is running then return true
                return true;
            }
        }
        return false;
    }
}
